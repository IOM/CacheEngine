﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CacheEngine;
using Memcached.ClientLibrary;
using CacheEngine.Memcache;
using CacheEngine.Redis;

namespace CacheEngineDemo1
{
    class Program
    {
        static void Main(string[] args)
        {

            //SockIOPool sockIoPool = SockIOPool.GetInstance();
            //sockIoPool.SetServers(new[] { "127.0.0.1:11211" });
            //sockIoPool.InitConnections = 3;
            //sockIoPool.MinConnections = 3;
            //sockIoPool.Initialize();

            //var client = new MemcachedClient();
            //var _engine = new MemcacheCacheEngine(client);
            var _engine2 = new RedisCacheEngine();
            string key = "ha666.trade."+4546464;
            int val = 88;
            _engine2.SaveOrUpdate(key, val, DateTime.Today.AddDays(1));

            int cachedVal = _engine2.Get<int>(key);

            var customerRepo = new DemoRepository();
            
            for (int i = 1; i < 6; i++)
            {
                string custName = string.Format("Customer {0}", i);
                Console.WriteLine("读取到的值是{0}", customerRepo.HitCache(x => x.GetCustomersAgeBy(custName)));
            }
            Boolean dddd = false;
            while (!dddd)
            {
                for (int i = 1; i < 6; i++)
                {
                    string custName = string.Format("Customer {0}", i);
                    Console.WriteLine("读取到的值是{0}，{1}", customerRepo.HitCache(x => x.GetCustomersAgeBy(custName)), DateTime.Now);
                }
                System.Threading.Thread.Sleep(5000);
            }


            Console.ReadKey();
        }
    }
}

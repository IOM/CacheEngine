﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace CacheEngine
{
    /// <summary>关键字生成器</summary>
    public interface IKeyGenerator
    {
        /// <summary>生成关键字</summary>
        /// <param name="objectType"></param>
        /// <param name="methodInfo"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        string GenerateKey(Type objectType, MethodInfo methodInfo, object[] args);
    }
}
